# Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

HOMEPAGE="http://telepathy.freedesktop.org"
if ever is_scm; then
    require github [ user=TelepathyIM ]
else
    DOWNLOADS="${HOMEPAGE}/releases/${PN}/${PNV}.tar.gz"
fi

require cmake [ api=2 ] test-dbus-daemon
export_exlib_phases src_test

SUMMARY="A high-level binding for Telepathy, similar to telepathy-glib but for Qt"

BUGS_TO="kde@exherbo.org"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

# Too many tests fail unreliably or hang
RESTRICT="test"

DEPENDENCIES="
    build:
        app-doc/doxygen
        dev-lang/perl:*
        dev-lang/python:*[>=2.5.0]
        virtual/pkg-config[>=0.21]
    build+run:
        dev-libs/glib:2
        dev-libs/libxml2:2.0[>=2.7.7-r1]
        media-libs/gstreamer:1.0
        net-im/farstream:0.2[>=0.2.0]
        net-im/telepathy-farstream[>=0.6.0]
        net-im/telepathy-glib[>=0.18.0] [[ note = [ tests and farstream ] ]]
        sys-apps/dbus
        x11-libs/qtbase:5
        x11-libs/qttools:5 [[ note = [ qhelpgenerator ] ]]
   test:
       (
            dev-python/dbus-python[>=0.83.1]
            x11-libs/qtbase:5[glib]
       ) [[ note = [ Run more tests ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DENABLE_EXAMPLES:BOOL=FALSE
    -DENABLE_FARSTREAM:BOOL=TRUE
    -DENABLE_SERVICE_SUPPORT:BOOL=TRUE
    -DFORCE_STATIC_SERVICE_LIBRARY:BOOL=FALSE
    -DDISABLE_WERROR:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_PARAMS+=( -DDESIRED_QT_VERSION=5 )
CMAKE_SRC_CONFIGURE_TESTS+=( '-DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE' )

telepathy-qt_src_test() {
    esandbox allow_net "unix-abstract:dbus-tube-test"
    esandbox allow_net "unix-abstract:/tmp/dbus-*"
    esandbox allow_net "unix:${TEMP%/}/dbus-*"
    esandbox allow_net "unix:/tmp/file*"
    esandbox allow_net "inet:0.0.0.0@0"
    esandbox allow_net "inet6:::@0"

    test-dbus-daemon_run-tests

    esandbox disallow_net "unix-abstract:dbus-tube-test"
    esandbox disallow_net "unix-abstract:/tmp/dbus-*"
    esandbox disallow_net "unix:${TEMP%/}/dbus-*"
    esandbox disallow_net "unix:/tmp/file*"
    esandbox disallow_net "inet:0.0.0.0@0"
    esandbox disallow_net "inet6:::@0"
}

