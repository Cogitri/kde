# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require option-renames [ renames=[ 'policykit polkit' ] ]
require kde-frameworks kde [ translations='qt' ] test-dbus-daemon

SUMMARY="Framework to let applications perform actions as a privileged user"
DESCRIPTION="
KAuth provides a convenient, system-integrated way to offload actions that need
to be performed as a privileged user (root, for example) to small (hopefully
secure) helper utilities."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
MYOPTIONS="polkit"

DEPENDENCIES="
    build+run:
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        polkit? ( sys-auth/polkit-qt:1[>=0.99.0][qt5(-)] )
    recommendation:
        polkit? (
            sys-auth/polkit-kde-agent[>=5.1.95] [[ description = [ KDE authentication GUI for PolicyKit ] ]]
        )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'polkit PolkitQt5-1' )

