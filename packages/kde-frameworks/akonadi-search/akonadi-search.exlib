# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

export_exlib_phases src_prepare

SUMMARY="Libraries and daemons to implement searching in Akonadi"

LICENCES="GPL-3 LGPL-2.1"
SLOT="5"
MYOPTIONS=""

KF5_MIN_VER=5.44.0
QT_MIN_VER=5.8.0

DEPENDENCIES="
    build+run:
        dev-db/xapian-core
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/kcalcore:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=${PV}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        !kde/baloo:4[-only-libraries(+)] [[
            description = [ Both akonadi-search and baloo:4 install akonadi_baloo_indexer ]
            resolution = uninstall-blocked-after
        ]]
"

akonadi-search_src_prepare() {
    kde_src_prepare

    # Disable tests which need X
    edo sed -e "/add_subdirectory(autotests)/d" \
            -i debug/CMakeLists.txt

    # TODO: Maybe these test could work without X, but I don't know how
    # to tell them that, because each test is run per supported DBMS.
    edo sed -e "/add_akonadi_isolated_test_advanced(schedulertest.cpp/d" \
            -e "/add_akonadi_isolated_test_advanced(collectionindexingjob/d" \
            -i agent/autotests/CMakeLists.txt
}

