# Copyright 2013, 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Miniature golf"
DESCRIPTION="
The game is played from an overhead view, with a short bar representing the golf club. Kolf features
many different types of objects, such water hazards, slopes, sand traps, and black holes (warps),
among others."
HOMEPAGE+=" http://kde.org/applications/games/${PN}/"

LICENCES="GPL-2 FDL-1.2 LGPL-2"
MYOPTIONS=""

KF5_MIN_VER=5.30.0
QT_MIN_VER=5.7.0

# NOTE: Annoyingly upstream bundles an old, modified copy of dev-libs/Box2D
DEPENDENCIES="
    build+run:
        kde/libkdegames:5[>=4.9.0]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5]
        x11-libs/qtbase:5
"

kolf_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kolf_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

